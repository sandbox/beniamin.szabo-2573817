CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers


INTRODUCTION
------------
The Commerce Coupon Extend module extends Commerce Coupon module providing 
extra conditions for coupons.

It adds two more inline conditions for coupons:

 1. Product - Restricts a coupon so it can only be used for a certain product 
    or multiple products.
 2. Product type - Restricts a coupon so it can only be used for a certain 
    product type.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/beniamin.szabo/2573817


 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2573817


REQUIREMENTS
------------
This module requires the following modules:
 * Commerce Coupon (https://www.drupal.org/project/commerce_coupon)
 * Inline Conditions (https://www.drupal.org/project/inline_conditions)


RECOMMENDED MODULES
-------------------
No recommended modules.


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration.


TROUBLESHOOTING
---------------


MAINTAINERS
-----------
Current maintainers:
 * Beniamin F. Szabo (https://www.drupal.org/u/beniamin.szabo)
