<?php

/**
 * @file
 * Rules definitions for Commerce Coupon Extend.
 */

/**
 * Implements hook_rules_condition_info().
 */
function cce_rules_condition_info() {
  $inline_conditions = inline_conditions_get_info();

  $conditions['cce_commerce_coupon_product_type'] = array(
    'label' => t('Coupon validation - Product type'),
    'description' => t('Enter the product type you want the coupon to be restricted on.'),
    'group' => t('Commerce Coupon'),
    'parameter' => array(
      'commerce_coupon' => array(
        'type' => 'commerce_coupon',
        'label' => t('Coupon'),
      ),
      'product_type' => array(
        'type' => 'text',
        'label' => t('Product type'),
      ),
    ),
    'callbacks' => array(
      'execute' => $inline_conditions['cce_commerce_coupon_product_type']['callbacks']['build'],
    ),
  );

  $conditions['cce_commerce_coupon_product'] = array(
    'label' => t('Coupon validation - Product'),
    'description' => t('Enter the product you want the coupon to be restricted on.'),
    'group' => t('Commerce Coupon'),
    'parameter' => array(
      'commerce_coupon' => array(
        'type' => 'commerce_coupon',
        'label' => t('Coupon'),
      ),
      'products' => array(
        'type' => 'list<integer>',
        'label' => t('Product(s)'),
      ),
    ),
    'callbacks' => array(
      'execute' => $inline_conditions['cce_commerce_coupon_product']['callbacks']['build'],
    ),
  );

  return $conditions;
}

/**
 * Build callback for cce_commerce_coupon_product_type.
 */
function cce_commerce_coupon_product_type_build($coupon, $product_type) {
  // Get current user's order.
  $order_wrapper = cce_commerce_order_wrapper();
  if (!$order_wrapper) {
    return FALSE;
  }

  // Loop trough order line items and check product type.
  foreach ($order_wrapper->commerce_line_items as $lineitem_wrapper) {
    if (property_exists($lineitem_wrapper->value(), 'commerce_product') && $lineitem_wrapper->commerce_product->type->value() === $product_type) {
      return TRUE;
    }
  }

  // Prepare the commerce coupon error message.
  $error = &drupal_static('commerce_coupon_error_' . strtolower($coupon->code));
  $error = t('This coupon is restricted to product type: @product_type.', array(
    '@product_type' => $product_type,
  ));

  return FALSE;
}

/**
 * Build callback for cce_commerce_coupon_product.
 */
function cce_commerce_coupon_product_build($coupon, $products) {
  // Get current user's order.
  $order_wrapper = cce_commerce_order_wrapper();
  if (!$order_wrapper) {
    return FALSE;
  }

  if (empty($products)) {
    return TRUE;
  }

  // Loop trough line items, check if there is at least one product.
  foreach ($order_wrapper->commerce_line_items as $wrapper_line_item) {
    // Ensures the passed line item is product type.
    if (in_array('commerce_product', array_keys($wrapper_line_item->getPropertyInfo()))) {
      $product_id = $wrapper_line_item->commerce_product->raw();
      if ($product_id && isset($products[$product_id])) {
        // We have a product, the coupon is valid.
        return TRUE;
      }
    }
  }

  // The coupon is not valid. Prepare the coupon error message.
  $product_titles = array();
  foreach (commerce_product_load_multiple($products) as $product) {
    $product_titles[] = check_plain($product->title);
  }

  $error = &drupal_static('commerce_coupon_error_' . strtolower($coupon->code));
  $error = t('This coupon is restricted to: @products_list.', array(
    '@products_list' => implode(', ', $product_titles),
  ));

  return FALSE;
}

/**
 * Helper: Get current user's order.
 */
function cce_commerce_order_wrapper() {
  global $user;

  $order = commerce_cart_order_load($user->uid);
  if (!$order) {
    return FALSE;
  }
  return entity_metadata_wrapper('commerce_order', $order);
}
