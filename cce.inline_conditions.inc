<?php

/**
 * @file
 * Provides Inline Conditions integration for the Commerce Coupon Extend module.
 */

/**
 * Implements hook_inline_conditions_info().
 */
function cce_inline_conditions_info() {
  $conditions = array();
  // Validate coupons by product type.
  $conditions['cce_commerce_coupon_product_type'] = array(
    'label' => t('Product type'),
    'entity type' => 'commerce_coupon',
    'callbacks' => array(
      'configure' => 'cce_commerce_coupon_product_type_configure',
      'build' => 'cce_commerce_coupon_product_type_build',
    ),
  );
  // Validate coupons by products.
  $conditions['cce_commerce_coupon_product'] = array(
    'label' => t('Product(s)'),
    'entity type' => 'commerce_coupon',
    'callbacks' => array(
      'configure' => 'cce_commerce_coupon_product_configure',
      'build' => 'cce_commerce_coupon_product_build',
    ),
  );

  return $conditions;
}

/**
 * Configuration callback for cce_commerce_coupon_product_type.
 */
function cce_commerce_coupon_product_type_configure($settings) {
  // Use the same form as commerce_discount does.
  $form = commerce_product_has_type_configure($settings);
  // Alter the help text.
  $form['type']['#suffix'] = '<div class="condition-instructions">' . t('The coupon is valid if the order contains a product type selected above.') . '</div>';

  return $form;
}

/**
 * Configuration callback for cce_commerce_coupon_product.
 */
function cce_commerce_coupon_product_configure($settings) {
  // Ensure we've default settings set.
  $settings += array(
    'products' => array(),
  );

  // Get values from $settings.
  $products_sku = array();
  if (!empty($settings['products'])) {
    foreach (commerce_product_load_multiple($settings['products']) as $product) {
      $products_sku[] = $product->sku;
    }
  }

  $form = array();

  $form['products'] = array(
    '#type' => 'textfield',
    '#title' => t('SKUs'),
    '#title_display' => 'invisible',
    '#default_value' => implode(', ', $products_sku),
    '#required' => TRUE,
    '#maxlength' => 4096,
    '#autocomplete_path' => 'commerce_product/autocomplete/commerce_product/0/0',
    // Use a custom element validate in order to maintain the array of products.
    '#element_validate' => array('cce_commerce_product_reference_autocomplete_validate'),
    '#suffix' => '<div class="condition-instructions">' . t('The coupon is valid if the order contains the following product(s).') . '</div>',
    '#attributes' => array('placeholder' => array(t('enter product name'))),
  );

  return $form;
}